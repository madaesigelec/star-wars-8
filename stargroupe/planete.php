<!DOCTYPE html>
<html>

   <head>
        <title>Planètes</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        
        <link type="text/css" rel="stylesheet" href="style.css">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>

        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>


    </head> 
    <header>
    
            <div style="background:transparent url('https://upload.wikimedia.org/wikipedia/commons/6/6c/Star_Wars_Logo.svg') no-repeat center center" class="row" >
                    <div class="col text-right">
                    <?php
  
       

  session_start();

if(isset($_SESSION["prenom"]) && $_SESSION['type']=='0'){?>
  
<div id=abc >
<style>
#abc{
  color : red;
}</style>
<h1 > Bienvenue  <?php echo $_SESSION['prenom']; ?> !!!</h1>
<p>C'est votre espace utilisateur.</p>
<a href='logout.php'>Déconnexion</a>
</div>
 
<?php } 

elseif(isset($_SESSION["prenom"]) && $_SESSION['type']=='1'){?>

<div id=abc >
<style>
#abc{
  color : red;
}</style>
<h1 > Bienvenue  <?php echo $_SESSION['prenom']; ?> !!!</h1>
<p>C'est votre espace administrateur.</p>
<a href='logout.php'>Déconnexion</a></p>
<a href="add.php">Ajouter un utilisateur</a></p>
<a href="addmovie.php">Ajouter un film</a></p>
<a href="addpeople.php">Ajouter un acteur</a></p>
</div>
<?php }

else{ ?>
 <a class="btn btn-outline-primary" href="https://moduleweb.esigelec.fr/grp_9_8/inscrire.php">S'inscrire</a>&nbsp;<a class="btn btn-outline-primary" href="https://moduleweb.esigelec.fr/grp_9_8/connecter.php">Se connecter</a> 
<?php }
    
    ?>

                    </div>                       
                     <div style="height: 230px;"  class="row justify-content-center">
                    </div>
            </div>
            <style>
                header {
                  background: rgb(2, 2, 2);
                }
                </style>
    </header> 
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="https://moduleweb.esigelec.fr/grp_9_8/#">Accueil <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="https://moduleweb.esigelec.fr/grp_9_8/film.php">Films<span class="sr-only">(current)</span></a>
              </li> 
            <li class="nav-item active">
                <a class="nav-link" href="https://moduleweb.esigelec.fr/grp_9_8/acteurs.php">Acteurs<span class="sr-only">(current)</span></a>
              </li>
            <li class="nav-item active">
                <a class="nav-link" href="https://moduleweb.esigelec.fr/grp_9_8/vaisseaux.php">Vaisseaux <span class="sr-only">(current)</span></a>
              </li>
            <li class="nav-item active">
                <a class="nav-link" href="https://moduleweb.esigelec.fr/grp_9_8/planete.php" >Planètes<span class="sr-only">(current)</span></a>
              </li>

          </ul>

        </div>
      </nav>
      <body >
        <style>
          body {
            background: rgb(54, 24, 49);
            color: white;
          }
          </style>
          <h1>La liste des planètes :</h1>
          <?php

require('config.php');
       
if (!$conn) {
    echo "Connect Error: " . mysqli_connect_error();
    exit();
}

      
      $sql = 'SELECT * FROM Planet'; 
      $query  = mysqli_query($conn, $sql); 
      $rowCount = mysqli_num_rows($query);
      
      
      if ($rowCount == 0) {
          echo "<h1>Aucune planète </h1>";
          exit();
      }
     
      while ($data = mysqli_fetch_array($query) ) { ?>

<div id="i"  class="row" >
          <style>
          #i{
            font-family: "Gill Sans", sans-serif;
            
            color: yellow;
            
            border-style:inset;
            border-color:black;
            
            margin: 15px ;
            padding:10px;

            
            
            
            background-image: url('https://img.freepik.com/photos-gratuite/fond-gris-peint_53876-94041.jpg?size=626&ext=jpg');
          }
          </style>
    
    <div class="col-4" id="al">
          <h2 >Nom : &emsp;
          <style>
        #al{
          text-align: center;
        }
          </style>

          <?php echo $data['name'];?> </h2>
           </div>
           <h3 id="pp" class="col-4">Population : &emsp; <?php echo $data['population'];?></h3>
           <style>
           #pp{
            margin: 10px auto;
            color : black;
            text-align: center;
           }
           </style>
      <div class="col-4">
      <?php 
if(isset($_SESSION["prenom"]) ){?>
           <div id="ds">
          <?php 
      echo "<a href='http://moduleweb.esigelec.fr/grp_9_8/detailplanete.php?nom=".$data['name']." '>Détails</a>"; ?>
          </div>
          <?php
         }  
        ?>


      </div>    
           
    </div>
      <?php }?>
      </body> 
    </html>