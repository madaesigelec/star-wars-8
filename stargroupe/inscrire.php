<!DOCTYPE html>
<html>

   <head>
        <title>Star Wars</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="style.css">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>

        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>


    </head> 
    <header>
    
            <div style="background:transparent url('https://upload.wikimedia.org/wikipedia/commons/6/6c/Star_Wars_Logo.svg') no-repeat center center" class="row" >
                    <div class="col text-right">
                    <?php
  
       

  session_start();

if(isset($_SESSION["prenom"]) && $_SESSION['type']=='0'){?>
  
<div id=abc >
<style>
#abc{
  color : red;
}</style>
<h1 > Bienvenue  <?php echo $_SESSION['prenom']; ?> !!!</h1>
<p>C'est votre espace utilisateur.</p>
<a href='logout.php'>Déconnexion</a>
</div>
 
<?php } 

elseif(isset($_SESSION["prenom"]) && $_SESSION['type']=='1'){?>

<div id=abc >
<style>
#abc{
  color : red;
}</style>
<h1 > Bienvenue  <?php echo $_SESSION['prenom']; ?> !!!</h1>
<p>C'est votre espace administrateur.</p>
<a href='logout.php'>Déconnexion</a></p>
<a href="add.php">Ajouter un utilisateur</a></p>
<a href="addmovie.php">Ajouter un film</a></p>
<a href="addplanet.php">Ajouter une planète</a></p>
</div>
<?php }

else{ ?>
 <a class="btn btn-outline-primary" href="https://moduleweb.esigelec.fr/grp_9_8/inscrire.php">S'inscrire</a>&nbsp;<a class="btn btn-outline-primary" href="https://moduleweb.esigelec.fr/grp_9_8/connecter.php">Se connecter</a> 
<?php }
    
    ?>

                    </div>                       
                     <div style="height: 230px;"  class="row justify-content-center">
                    </div>
            </div>
            <style>
                header {
                  background: rgb(2, 2, 2);
                }
                </style>
    </header> 
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="https://moduleweb.esigelec.fr/grp_9_8/#">Accueil <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="https://moduleweb.esigelec.fr/grp_9_8/film.php">Films<span class="sr-only">(current)</span></a>
              </li> 
            <li class="nav-item active">
                <a class="nav-link" href="https://moduleweb.esigelec.fr/grp_9_8/acteurs.php">Acteurs<span class="sr-only">(current)</span></a>
              </li>
            <li class="nav-item active">
                <a class="nav-link" href="https://moduleweb.esigelec.fr/grp_9_8/vaisseaux.php">Vaisseaux <span class="sr-only">(current)</span></a>
              </li>
            <li class="nav-item active">
                <a class="nav-link" href="https://moduleweb.esigelec.fr/grp_9_8/planete.php" >Planètes<span class="sr-only">(current)</span></a>
              </li>

          </ul>

        </div>
      </nav> 
      <body>
      <style>
          body {
            background: rgb(54, 24, 49);
            color: white;
          }
          </style>
    <?php
require('config.php');
if (isset($_REQUEST['prenom'], $_REQUEST['nom'], $_REQUEST['mdp'])){
  // récupérer le nom d'utilisateur et supprimer les antislashes ajoutés par le formulaire
  $prenom = stripslashes($_REQUEST['prenom']);
  $prenom = mysqli_real_escape_string($conn, $prenom); 
  // récupérer l'email et supprimer les antislashes ajoutés par le formulaire
  $nom = stripslashes($_REQUEST['nom']);
  $nom = mysqli_real_escape_string($conn, $nom);
  // récupérer le mot de passe et supprimer les antislashes ajoutés par le formulaire
  $mdp = stripslashes($_REQUEST['mdp']);
  $mdp = mysqli_real_escape_string($conn, $mdp);
  //requéte SQL + mot de passe crypté
    $query = "INSERT into `Utilisateur` (prenom, nom, mdp,type)
              VALUES ('$prenom', '$nom', '".hash('sha256', $mdp)."','0')";
  // Exécuter la requête sur la base de données
    $res = mysqli_query($conn, $query);
    if($res){
       echo "<div class='sucess'>
             <h3>Vous êtes inscrit avec succès.</h3>
             <p>Cliquez ici pour vous <a href='connecter.php'>connecter</a></p>
       </div>";
    }
}else{
?>
<form class="box" action="" method="post">
  
    <h1 class="box-title">S'inscrire</h1>
  <input type="text" class="box-input" name="prenom" placeholder="Prénom d'utilisateur" required />
    <input type="text" class="box-input" name="nom" placeholder="Nom" required />
    <input type="text" class="box-input" name="mdp" placeholder="Mot de passe" required />
    <input type="submit" name="submit" value="S'inscrire" class="box-button" />
    <p class="box-register">Déjà inscrit? <a href="https://moduleweb.esigelec.fr/grp_9_8/connecter.php">Connectez-vous ici</a></p>
</form>
<?php } ?>
      </body>
</html>